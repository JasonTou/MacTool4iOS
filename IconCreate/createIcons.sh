#!/bin/sh
#本文件的作用是 根据 icon-1024.png 的 icon ,生成
# Icon-57.png
# Icon-72.png
# Icon-76.png
# Icon-114.png
# Icon-120.png
# Icon-144.png
# Icon-152.png
# icon.png     ----  57*57
# icon@2x.png  ---- 114*114

#使用方式: 在脚本文件的统计目录，放入  icon-1024.png 文件.然后执行脚本

defaultIconName="icon-1024.png"

#获取.sh文件的路径
shFilePath="noPathYet"

cd `dirname $0`
#echo `basename $0` is in `pwd`
shFilePath=$(pwd)
cd -

cd $shFilePath
templateIcon="$shFilePath/$defaultIconName"
echo "TemplateIcon: $templateIcon"

# 判断是否有入参设定的源图片，没有就使用默认的
oriIconName=""
if [ -n "$1" ]
then
	oriIconName="$1"
else
	oriIconName="$defaultIconName"
fi

echo "source image is $oriIconName"

function iOS_process()
{
# rm -f "Icon-57.png"
# sips -s format png -z 57 57 $oriIconName --out "Icon-57.png"
# 
# rm -f "Icon-76.png"
# sips -s format png -z 76 76 $oriIconName --out "Icon-76.png"
# 
# rm -f "Icon-72.png"
# sips -s format png -z 72 72 $oriIconName --out "Icon-72.png"
# 
# rm -f "Icon-114.png"
# sips -s format png -z 114 114 $oriIconName --out "Icon-114.png"
# 
# rm -f "Icon-120.png"
# sips -s format png -z 120 120 $oriIconName --out "Icon-120.png"
# 
# rm -f "Icon-144.png"
# sips -s format png -z 144 144 $oriIconName --out "Icon-144.png"
# 
# rm -f "Icon-152.png"
# sips -s format png -z 57 57 $oriIconName --out "Icon-152.png"
# 
# rm -f "icon.png"
# sips -s format png -z 152 152 $oriIconName --out "icon.png"
# 
# rm -f "icon@2x.png"
# sips -s format png -z 114 114 $oriIconName --out "icon@2x.png"
# 
# rm -f "iTunesArtwork"
# sips -s format png -z 512 512 $oriIconName --out "iTunesArtwork"

# iOS 图片
# 创建iOS文件夹
	ios_dir="iOS"
	rm -fr $ios_dir
	mkdir -p $ios_dir

# 配置输出大小
	iosIconSizeArray=(29 40 50 57 58 72 76 80 87 100 114 120 144 152 180 )
	watchIconSizeArray=(48 55 80 88 172 196)
	arrayLen=${#iosIconSizeArray[@]}
	for ((i=0;i<arrayLen;i++))
	do
		size=${iosIconSizeArray[i]} 
		sips -s format png -z $size $size $oriIconName --out "$ios_dir/Icon-$size.png"
	done
	arrayLen=${#watchIconSizeArray[@]}
	for ((i=0;i<arrayLen;i++))
	do
		size=${watchIconSizeArray[i]} 
		sips -s format png -z $size $size $oriIconName --out "$ios_dir/watch-$size.png"
	done
# 三张特殊的图片
	sips -s format png -z 152 152 $oriIconName --out "$ios_dir/icon.png"
	sips -s format png -z 114 114 $oriIconName --out "$ios_dir/icon@2x.png"
	sips -s format png -z 512 512 $oriIconName --out "$ios_dir/iTunesArtwork"
}

function android_process()
{
# Android 图片
	android_dir="Android"
	android_icon_name="icon.png"
	rm -fr $android_dir
	mkdir -p $android_dir

# # 48*48图片
# drawable_dir="drawable"
# mkdir -p "$android_dir/$drawable_dir"
# sips -s format png -z 48 48 $oriIconName --out "$android_dir/$drawable_dir/$android_icon_name"
# 
# # 72*72图片
# drawable_hdpi_dir="drawable-hdpi"
# mkdir -p "$android_dir/$drawable_hdpi_dir"
# sips -s format png -z 72 72 $oriIconName --out "$android_dir/$drawable_hdpi_dir/$android_icon_name"
# 
# # 44*44图片
# drawable_ldpi_dir="drawable-ldpi"
# mkdir -p "$android_dir/$drawable_ldpi_dir"
# sips -s format png -z 44 44 $oriIconName --out "$android_dir/$drawable_ldpi_dir/$android_icon_name"
# 
# # 48*48图片
# drawable_mdpi_dir="drawable-mdpi"
# mkdir -p "$android_dir/$drawable_mdpi_dir"
# sips -s format png -z 48 48 $oriIconName --out "$android_dir/$drawable_mdpi_dir/$android_icon_name"
# 
# # 96*96图片
# drawable_xhdpi_dir="drawable-xhdpi"
# mkdir -p "$android_dir/$drawable_xhdpi_dir"
# sips -s format png -z 96 96 $oriIconName --out "$android_dir/$drawable_xhdpi_dir/$android_icon_name"

	dirArray=("drawable" "drawable-hdpi" "drawable-ldpi" "drawable-mdpi" "drawable-xhdpi" "drawable-xxhdpi" "drawable-xxxhdpi")
	sizeArray=(48 72 44 48 96 144 192)

	arrayLen=${#dirArray[@]}
	for ((i=0;i<arrayLen;i++))
	do
		icon_dir="$android_dir/${dirArray[i]}"
		# 创建目录
		mkdir -p $icon_dir
		icon_size=${sizeArray[i]}
		# 生成图片
		sips -s format png -z $icon_size $icon_size $oriIconName --out "$icon_dir/$android_icon_name"
	done
}

if [ -z $2 ]; then
	iOS_process
	android_process
elif [ $2 == "iOS" ]; then
	iOS_process
elif [ $2 == "android" ]; then
	android_process
fi
