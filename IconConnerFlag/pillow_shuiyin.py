#!/usr/bin/python
# encoding=utf-8
import sys, argparse
try:
    from PIL import Image, ImageDraw, ImageFont, ImageEnhance
except ImportError:
    import Image, ImageDraw, ImageFont, ImageEnhance

def logo_watermark(image_path, logo_path, outFilePath):
    '''
    增加一个图片水印，原理就是合并图片层
    '''
    baseim = Image.open(image_path).convert('RGBA')
    logoim = Image.open(logo_path)
    logoim = logoim.convert('RGBA')
    
    bw, bh = baseim.size
    lw, lh = logoim.size

    layer = Image.new('RGBA', baseim.size, (0, 0, 0, 0))
    layer.paste(logoim, (0, 0))

    baseim = Image.composite(layer, baseim, layer)
    baseim.save(outFilePath, 'PNG')
    print u'添加水印成功'

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('logoFile', help="待处理的 ICON 源文件，在 ICON 文件夹下")
    parser.add_argument('waterMarkFile', help="对应的水印文件名，在目录 WATERMARK 下")
    parser.add_argument('outputFile', help="输出的文件名，在 out 目录下")
    args = parser.parse_args();

    logoFile = 'ICON/' + args.logoFile
    waterMarkFile = 'WATERMARK/' + args.waterMarkFile
    outputFile = 'out/' + args.outputFile
    logo_watermark(logoFile, waterMarkFile, outputFile)


